<?php

namespace Drupal\duk_tools\Commands;

use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class DukToolsCommands extends DrushCommands {

  /**
   * Generate a new Doghouse starter kit theme.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases,
   *   config, etc.
   *
   * @option array option-name
   *   Description
   * @usage duk_tools-subTheme foo
   *   Usage description
   *
   * @command duk_tools:subTheme
   * @aliases duk
   */
  public function subTheme(array $options = ['option-name' => 'default']) {
    $this->logger()->notice(dt('Use `drush gen duk` to create a new duk subtheme.'));
  }

}
