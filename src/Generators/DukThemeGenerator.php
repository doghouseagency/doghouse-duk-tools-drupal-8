<?php

namespace Drupal\duk_tools\Generators;

use DrupalCodeGenerator\Command\BaseGenerator;
use DrupalCodeGenerator\Utils;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\ChoiceQuestion;

/**
 * Drush theme generator.
 */
class DukThemeGenerator extends BaseGenerator {
  protected $name = 'duk-theme';
  protected $description = 'Generates a cog theme.';
  protected $alias = 'duk';
  protected $templatePath = __DIR__ . "/../../templates";
  protected $starterkitDir = 'starterkit';
  protected $destination = 'themes';

  /**
   * Prompt the user for desired theme options.
   */
  protected function interact(InputInterface $input, OutputInterface $output) {
    $questions['name'] = new Question('Theme name');
    $questions['machine_name'] = new Question('Theme machine name');
    $questions['base_theme'] = new Question('Base theme', 'classy');
    $questions['description'] = new Question('Description', 'Doghouse starter theme');
    $questions['package'] = new Question('Package', 'Custom');

    $vars = $this->collectVars($input, $output, $questions);

    $output->writeln('Creating sub theme!');

    $vars['base_theme'] = Utils::human2machine($vars['base_theme']);

    // Where (inside themes/) to put this stuff, hardcoded for now.
    $location = 'custom/';

    $prefix = $vars['machine_name'] . '/' . $vars['machine_name'];

    // Core duk stuff.
    $this->addFile()
      ->path($location . '{machine_name}/README.md')
      ->template('starterkit/README.md');

    $this->addFile()
      ->path($location . $prefix . '.info.yml')
      ->template('starterkit/theme-info.twig');

    $this->addFile()
      ->path($location . '{machine_name}/logo.svg')
      ->template('starterkit/theme-logo.twig');

    $this->addFile()
      ->path($location . $prefix . '.libraries.yml')
      ->template('starterkit/theme-libraries.twig');

    $this->addFile()
      ->path($location . $prefix . '.breakpoints.yml')
      ->template('starterkit/breakpoints.twig');

    $this->addFile()
      ->path($location . $prefix . '.theme')
      ->template('starterkit/theme.twig');

    // Front end tools.
    $this->addFile()
      ->path($location . '{machine_name}/.eslintignore')
      ->template('starterkit/.eslintignore');

    $this->addFile()
      ->path($location . '{machine_name}/.stylelintrc.json')
      ->template('starterkit/.stylelintrc.json');

    // Node and nvm install script.
    $this->addFile()
      ->path($location . '{machine_name}/install-node.sh')
      ->template('starterkit/install-node.sh');

    // Copy directories.
    $dirs_to_copy = [
      'fonts',
      'images',
      'js',
      'layouts',
      'styles',
      'templates',
      'build-boilerplate',
    ];

    foreach ($dirs_to_copy as $dir) {
      $source = $this->templatePath . '/starterkit/' . $dir;
      $dest = $location . '{machine_name}/' . $dir;
      $this->copyDirRecursive($source, $dest);
    }

    $output->writeln('----------------------------------------------');
    $output->writeln('I have created some build boilerplates for you!');
    $output->writeln('Move the contents of "{machine_name}/build-boilerplates" to the root of  your repo and then "npm install".');
  }

  /**
   * A wrapper for copying a directory and all its files recursively.
   *
   * It uses Drush methods found in `BaseGenerator` to correctly apply template
   * variables and processing where required. see `addFile()`. AddFile expects
   * template dir to be relative so this handles the conversion too.
   *
   * @param $src string
   *   Absolute path to source dir.
   * @param $dst
   *   Absolute path to destination dir.
   */
  protected function copyDirRecursive($src, $dst) {
    $dir = opendir($src);
    $this->addDirectory()->path($dst);
    while (FALSE !== ($file = readdir($dir))) {
      if (( $file != '.' ) && ( $file != '..' )) {
        // Is dir.
        if ( is_dir($src . '/' . $file) ) {
          $this->copyDirRecursive($src . '/' . $file,$dst . '/' . $file);
        }
        else {
          // Is file.
          // Convert file to template location.
          $src_delimiter = $this->starterkitDir . DIRECTORY_SEPARATOR;
          $template_src_parts = explode($src_delimiter, $src . '/' . $file);
          $template_src = $src_delimiter . $template_src_parts[1];
          // Save the file.
          $this->addFile()
            ->path($dst . '/' . $file)
            ->template($template_src);
        }
      }
    }
    closedir($dir);
  }

}
