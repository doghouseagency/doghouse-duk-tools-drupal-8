# Duk Tools

A Drupal 8 sub theme generator, with build tools. Enables sub theme generation via drush 9. 

Forked and adapted from Acquia [Cog Tools](https://github.com/acquia-pso/cog_tools) to suit Doghouse projects.

## Quickstart:

Add doghouse packages repo.

`composer config repositories.doghouse composer http://packages.doghouse.agency/`

Download the duk_tools module.
 
`composer require drupal/duk_tools`
 
Enable the duk_tools module

`drush pm:enable duk_tools`

Create a sub theme with drush.

`drush generate duk`

Answer the questions.

Enable your new sub theme. For a theme with the machine name `my_theme`:

`drush theme:enable my_theme`

## Advanced topics

Passing in arguments via the command line:

`drush gen duk --answers '{"name":"Durian", "machine_name": "durian", "base_theme": "classy", "description": "What a nice theme.", "package": "Custom"}'`

Any answers that are left off here will be asked still, so this could be handy if you have a few options you almost always select.
