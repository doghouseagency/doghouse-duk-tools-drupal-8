const { createConfig } = require('@doghouse/webpack-base');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const path = require('path');
const argv = require('yargs').argv;

// Prevent JS being created for CSS only entry points.
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries");

// Paths (theme)
// -------------------------
const themeRelPath = '/themes/custom/{{ machine_name }}';
const themeAbsPath = path.resolve(__dirname, 'docroot/' + themeRelPath);
const themeAbsPathStyles = themeAbsPath + '/styles';
const themeAbsPathJs = themeAbsPath + '/js';

// Style guide (KSS config).
// -------------------------
// All projects should have a style guide, but if you don't want one then remove below, KSS reference
// in plugins and KSS reference in your package.json.
const KssWebpackPlugin = require('kss-webpack-plugin');
const kssOptions = {
  source: themeAbsPathStyles + '/styles/scss',
  css: themeRelPath + '/css/main.css',
  destination: path.resolve(__dirname, 'docroot/styleguide'),
  markup: true,
  title: '{{ name }}',
  homepage: 'style-guide-only/homepage.md',
};

// Add export config.
// -------------------------
// Each object added to this should be wrapped by createConfig() to merge in base config.
// Eg. module.exports.push(createConfig({ ... })
module.exports = [];

// SCSS.
let cssConfig = {
  entry: {
    // Entry point for SCSS to compile.
    main: [themeAbsPathStyles + '/scss/main.scss'],
  },
  output: {
    // Dir that CSS gets saved to.
    path: themeAbsPathStyles + '/css',
  },
  plugins: [
    // Prevent webpack generating JS in the CSS dir.
    new FixStyleOnlyEntriesPlugin(),
    // Clear CSS dir before building.
    new CleanWebpackPlugin(themeAbsPathStyles + '/css'),
  ],
});

// KSS Style guide build if specified with --styleguide arg.
if argv.styleguide {
  cssConfig.plugins.push(new KssWebpackPlugin(kssOptions))
}

// Add css to module exports.
module.exports.push(createConfig(cssConfig));

// Javascript.
// UNCOMMENT BELOW TO ENABLE JS PROCESSING (eg. Babel)
// module.exports.push(createConfig({
//   entry: {
//     main: [themeAbsPathJs + '/theme.js'],
//   },
//   output: {
//     path: themeAbsPathJs + '/dist',
//   },
//   plugins: [
//     new CleanWebpackPlugin(themeAbsPathJs + '/dist'),
//   ],
// }));
