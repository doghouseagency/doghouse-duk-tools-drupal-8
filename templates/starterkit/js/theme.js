(function ($, Drupal) {

  /**
   * {{ name }} Example JS.
   *
   * https://www.drupal.org/docs/8/api/javascript-api/javascript-api-overview
   */
  Drupal.behaviors.{{ machine_name }}ExampleBehaviour = {
    attach: function (context, settings) {

      // Always pass `context` when using jQuery selectors, this aids performance and
      // ensures your JS is applied to elements added via ajax.
      var $ctx = $('body', context);

      // Always use `.once()` to ensure JS only binds once to an element. It is not
      // uncommon for `.attach()` to get called multiple times. Partner with `each()`
      // to bind to each element once. Once arg should always be unique.
      $ctx.once('my-theme-base-init').each(function(){

        // Always detect features before using them.
        // if (window.console) {
        //  console.log('Doghouse base theme initialized');
        // }
      });
    }
  };

}(jQuery, Drupal));
